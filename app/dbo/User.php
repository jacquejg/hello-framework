<?php

namespace app\dbo;

/**
 * Description of User
 *
 * @author jacquelyng
 */
class User extends \rueckgrat\db\Mapper {
    
    protected $prename;
    protected $name;
    protected $mail;


    public function __construct() {
        parent::__construct();
    }

    function getPrename() {
        return $this->prename;
    }

    function getName() {
        return $this->name;
    }

    function getMail() {
        return $this->mail;
    }

    
}
