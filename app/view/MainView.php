<?php

namespace app\view;

/**
 * Description of MainView
 *
 * @author jacquelyng
 */
class MainView extends \rueckgrat\mvc\FastView {
    
    protected $users;
    protected $user;
    
    public function __construct() {
        parent::__construct();
        
        $this->cacheDisabled = TRUE;
        $this->title = 'Hello Framework';
    }
    
    public function renderFrontPage($users) {
        $this->users = $users;
        $this->pageContent = $this->getCompiledTpl("main/main");
    
        return $this->renderMainPage();
    }
    
    public function renderEditPage(\app\mapper\User $user) {
        $this->user = $user;
        $this->pageContent = $this->getCompiledTpl("main/edit");
    
        return $this->renderMainPage();
    }
}
