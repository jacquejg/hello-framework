<?php

namespace app\validator;

use rueckgrat\security\ValidationRules;
use rueckgrat\security\ValidationRule;

/**
 * Description of UserValidator
 *
 * @author jacquelyng
 */
class UserValidator extends \rueckgrat\security\ValidatorContainer {
    
    public function __construct(\app\mapper\User $user) {
        parent::__construct($user);
        
        $prename = new ValidationRule('prename', ValidationRules::MIXED);
        $prename->setLengths(3, 30);
        $prename->setErrorMsgGlobal("Please enter a prename");
        
        $this->addRule($prename);
        
        $name = new ValidationRule('name', ValidationRules::MIXED);
        $name->setLengths(3, 30);
        $name->setErrorMsgGlobal("Please enter a name");
        
        $this->addRule($name);
        
        $mail = new ValidationRule('mail', ValidationRules::EMAIL);
        $mail->setLengths(3, 100);
        $mail->setErrorMsgGlobal("Please enter a mail");
        
        $this->addRule($mail);
    }
}
