<!DOCTYPE html>
<html>
    <head>
        <title><?= $this->title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        [<?php
        foreach ($this->cssFiles AS $file) {
            echo '<link rel="stylesheet" type="text/css" href="' . $file . '">' . "\n";
        }
        ?>]
    </head>
    <body>
        <div id="wrapper">
            [<?php echo $this->pageContent; ?>]
        </div>
        <?php
        $cb = \rueckgrat\xhr\CallbackManager::getCallbacks();
        if (count($cb) > 0) {
            echo '<script type="text/javascript"">$(document).ready(function(){handleCallbacks(' . json_encode(array('callbacks' => $cb)) . ');});</script>' . "\n";
        }
        ?>
        [<?php
        foreach ($this->jsFiles AS $file) {
            echo '<script type="text/javascript" src="' . $file . '"></script>' . "\n";
        }
        ?>]
        <script type="text/javascript" src="public/js/main.js"></script>
    </body>
</html>