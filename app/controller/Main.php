<?php

namespace app\controller;

use rueckgrat\mvc\DefaultController;
use rueckgrat\security\Input;

/**
 * Description of Main
 *
 * @author jacquelyng
 */
class Main extends DefaultController {
    
    protected $userModel;
    protected $mainView;

    public function __construct() {
        parent::__construct();
        
        $this->userModel = new \app\model\UserModel();
        $this->mainView = new \app\view\MainView();
    }
    
    public function index() {
        $users = $this->userModel->getAllUsers();
        
        return $this->mainView->renderFrontPage($users);
    }
    
    public function create() {
        $user = new \app\mapper\User();
        $user->map(array(
            'prename' => Input::p('prename'),
            'name' => Input::p('name'),
            'mail' => Input::p('mail')
        ));
        $this->userModel->createUser($user);
        
        header('Location: ?c=Main&m=index');
    }
    
    public function delete() {
        $user = new \app\mapper\User();
        $user = $this->userModel->getById(Input::g('id'));
        $this->userModel->deleteUser($user);
        
        header('Location: ?c=Main&m=index');
    }
    
    public function edit() {
        $user = new \app\mapper\User();
        $user = $this->userModel->getById(Input::g('id'));
        
        return $this->mainView->renderEditPage($user);
    }
    
    public function editUser() {
        $user = new \app\mapper\User();
        $user->map(array(
            'id' => Input::p('id'),
            'prename' => Input::p('prename'),
            'name' => Input::p('name'),
            'mail' => Input::p('mail')
        ));
        
        $this->userModel->editUser($user);
        header('Location: ?c=Main&m=index');
    }
}
